#include <iostream>
#include <time.h>

// The method returns the current date.
int currentDay()
{
	time_t theTime = time(NULL);
	struct tm* aTime = std::localtime(&theTime);
	return aTime->tm_mday;
}

int main()
{
	int day = currentDay();
	int sumRows = 0;
	const int N = 4;
	int array[N][N];

	// We fill the array like this: element with indices i and j = i + j
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			array[i][j] = i + j;
			std::cout << array[i][j] << ' ';
		}
		std::cout << '\n';
	}

	std::cout << '\n';

	/*
	We display the sum of the elements of the array row,
	the index of which is equal to the remainder of dividing 
	the current calendar number by N.
	*/
	for (int i = 0; i < N; i++)
	{
		sumRows += array[day % N][i];
	}
	std::cout << "Sum of line elements: " << sumRows << '\n';
}